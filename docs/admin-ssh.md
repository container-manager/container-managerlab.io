---
id: admin-ssh
title: SSH
sidebar_label: SSH
---

This page describes some common operations available to SSH admins.

## Creating Projects

Run the following command to create a project:

`group create --comment @project <project-name>`

When creating a project, you should also provide tags in commma separated form to the `--tags` option.

To create a local only project, you would run:

`group create --comment @project --tags local <project-name>`

To create a local and nfs project, you would run:

`group create --comment @project --tags nfs,local <project-name>`

You can update the tags with the `group update` command.

> If you wish to configure NFS resources, you'll need to be a server admin. If you configure the `nfs` tag without having the resource mounted, `project attach` will fail.

## Adding and Removing Users

SSH admins can manually add and remove users. If you need to remove a user, you must remove their containers (via the containers shell) before you delete the user.

## Container Shell Operations

When an SSH admin is using the containers shell, they can activate admin mode when running commands by specifying the `-a` flag. When using the `-a` flag with the `ls` command, all containers will be listed. When using the `-a` flag with commands other commands, you can take actions on containers you don't own.

> When running commands in admin mode, you must use the internal name rather than the container name.

## Validating Hardware Tokens

If a user asks you to validate their hardware token, follow these steps:

1. Ensure that the user has already used the `register` shell to register their token.
2. Obtain the public key of the token
    - If using a PIV/yubikey, insert it into your computer. Run `pkcs15-tool --list-public-keys` to obtain the public keys on the token.
    - If using `sekey`, run `sekey -l` then `sekey -e <id>` to retrieve the public key
3. Run `userkey ls --user <username>` at the admin shell to list the currently registered SSH keys for the user
4. Find id for the matching public key
5. Run `userkey update --settoken <id>`

## Host Operations

SSH admins can set and unset access restrictions on hosts. When a host is access restricted, the user must be in at least one of the same groups as the host to access it. Here's how you could restrict access to a host.

1. Run `host update --setrestricted <hostname>`
2. Run `group update --addhost <hostname>`
3. Run `group update --adduser <username>`

> Remember that project admins can freely add and remove users from a group. We suggest delegating this task to a project admin.

You can also set the login banner via the `host update --setbanner "<banner>"` command. This message will be displayed to users when the login. It's useful for notifying users of upcoming maintenance. 

> You can use `\r\n` to insert newlines in your banner.