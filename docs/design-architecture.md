---
id: design-architecture
title: Architecture
sidebar_label: Architecture
---

This page describes the architecture of the system.

## Overview

We use `ssh-portal` to manage SSH connections and provide interactive meta shells. We use `lxd` to provide management of containers. We use `gpu-manager` to manage GPU leases inside containers.

## ssh-portal

The core functionality of `ssh-portal` is to act as a transparent SSH bastion host. It performs the following bastion actions:

1. Accepts an encrypted SSH connection
2. Inspects the connection to determine the target (based on username) and user (based on ssh key)
3. Decides whether the connection should continue based on group membership
4. Establishes a new SSH connection to the remote target (container)
5. Proxies SSH requests and channels bidirectionally between the user and the remote target

`ssh-portal` also provides several interactive shells to perform various meta tasks (`register`, `containers`, `admin`). When you SSH to one of these shells, the following steps change:

4. Accept the first channel (expected to be type `shell`)
5. Create an [ishell](https://github.com/abiosoft/ishell) instance of the appropriate shell
6. Bidirectionally pipe IO between the SSH channel and the ishell instance

One could easily add additional shells. I've considered adding shells for GPU/host stats (htop output?).

When at the `containers` shell, most interactive commands result in database and [`lxd` api calls](https://godoc.org/github.com/lxc/lxd/client). When you run `create` that roughly translates to `lxc launch`. We provide additional access controls and permission management that are absent in both SSH and `lxd`.

The bastion functionality of `ssh-portal` was conceptually based on work done by [Manfred Touron](https://github.com/moul/sshportal). The code was entirely written from scratch to better support our use cases.

## gpu-manager

`gpu-manager` maintains a list of all the GPUs on the host and services requests to `get`, `release`, and `wait`. The `gpu-manager` service is also responsible for sending stats messages (both GPU and CPU/Disk) to the status server (if configured). `gpu-manager` listens on a unix socket which is bind mounted into all containers. 

`gpu-manager` also contains a cli program (`gpu`) which issues requests to the `gpu-manager` server.

Here is what happens when a user executes `gpu get 1` inside a container:

1. `gpu` constructs an HTTP POST request to the `/get` endpoint
2. The request is send to the `/dev/gpu-manager/sock` socket
3. The request is received by the server
4. The server retrieves the requesters PID via the SCM_CREDENTIALS unix socket structure
5. The server looks up the requestors cgroup via `/proc/<pid>/cgroup`.
    - This is the `lxd` name of the container
6. If the requested number of GPUs are greater than those available, the request is rejected
7. One GPU is removed from the available pool
8. A `lxd` API call is issued to assign the GPU to the container
9. A stats message is constructed and sent to the stats server
10. The GPU is monitored for usage in the background