---
id: user-hpc-migration
title: HPC Migration
sidebar_label: HPC Migration
---

In this document, we'll discuss how you could migrate a docker container to a singularity container running on a HPC cluster. We assume you want to run GPU enabled code. We use `gpu-test` as a placeholder for your docker container name. We will use `login01` as a placeholder for the cluster's login node hostname.

The goal is to do experimentation and testing inside this system, then deploy to a HPC cluster for more compute power.

Review the [docker tips](user-tips.md#docker) before starting.

## Building a Docker Container

We start by building and testing a docker container. If you need help with docker, check out [this](https://docs.docker.com/get-started/part2/) page. Here's a sample Dockerfile which would build a simple docker container to test tensorflow:

```
FROM tensorflow/tensorflow:latest-gpu

RUN mkdir /app
WORKDIR /app
COPY test.py test.py

CMD python2 /app/test.py
```

> The `FROM tensorflow/tensorflow:latest-gpu` installs tensorflow globally. You could alternatively use `FROM nvidia/cuda:10.0-cudnn7-runtime` to customize your tensorflow version.

Here's what my test.py looks like:

```python
import tensorflow as tf
sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))
```

To build it, `cd` into the directory containing the Dockerfile and run `docker build . -t gpu-test`

## Testing a Docker Container

Once you've built a docker image, you should ensure that it works in our environment. Perform the following steps:

- `gpu get --lock 1`
- `docker run --runtime=nvidia --rm -it gpu-test`

## Migrating a Container

We can use the `docker save` command to export a tarball of the container rootfs. However, the `docker save` command will only work on a container (not an image). Therefore, we need to create a container before we run `docker save`.

`docker create --name gpu-test gpu-test`

On the login node, make a directory for the container rootfs.

`mkdir gpu-test`

Now we export the container rootfs to our HPC login node.

`docker export gpu-test | ssh username@login01 tar x -C gpu-test`

> This operation will take some time

When this operation finishes, run `ls gpu-test` on the login node. You should see top level linux directories (`bin`, `usr`, `lib`, etc).

## Testing a Container

Before scheduling our container to run on the cluster, we should test it on the login node. Run the following command to test your code:

`singularity exec --nv gpu-test python2 /app/test.py`

> A warning like `no CUDA-capable device is detected` is expected since login nodes don't have GPUs

## Scheduling a Container

The following `msub` script will use execute your container on the cluster:

```
#!/bin/bash

#MSUB -l nodes=1:gpus=1,walltime=300
cd $PBS_O_WORKDIR

singularity exec --nv ./gpu-test python2 /app/test.py
echo "Job submitted by $PBS_O_LOGNAME ran on $HOSTNAME"
```

## Additional Notes

This section contains more information on other use cases.

### Tuning Your Code

Unless you need to update libraries, you don't need to copy your whole container again. Example:

`vim gpu-test/app/test.py`

### Bind Mounts

Docker uses `-v <host-path>:<guest-path>` to bind a host directory into the container. 

Singularity uses `-B <host-path>:<guest-path>`.

### gzip

You could also gzip your container in the [Migrating a Container](#migrating-a-container) step.

`docker export gpu-test | gzip | ssh username@login01 tar xz -C gpu-test`

### rootfs on NFS

Most clusters have the home directories mounted on NFS. Extraction of the rootfs is particularly slow because we extract on NFS. There is also the runtime overhead of several nodes accessing the same files on job startup. However, our cluster is small enough where I'm not particularly worried about the performance.

### Directory vs simg

Singularity won't work inside an unprivileged container because it uses `/dev/loop`. The most we can do in our container environment is export a tarball.

You could add an additional step to build a `.simg` after you've extracted the tarball on the login node. In this case, you should extract the rootfs to `/scratch/gpu-test` to speed up the extraction. Here's what that process would look like:

`docker save gpu-test | ssh username@login01 tar x -C /scratch/gpu-test`

`singularity build gpu-test.simg /scratch/gpu-test`

`singularity exec gpu-test.simg python2 /app/test.py`

If a system doesn't have `/scratch/` available, `/tmp/` could also be used with sufficient space. 
