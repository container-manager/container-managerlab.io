---
id: user-gpu-manager
title: GPU Manager
sidebar_label: GPU Manager
---

Once you have created your container, you can use the `gpu` command to `get` and `release` GPUs. While this command may be present on non-GPU systems, it will not function fully. Here's an example of what this looks like:

```shell
➜  ~ ssh test@gpu.mitre.org
agartner@test:~$ nvidia-smi
No devices were found
agartner@test:~$ gpu get 1
OK
agartner@test:~$ nvidia-smi
Wed Nov 21 18:11:54 2018
+-----------------------------------------------------------------------------+
| NVIDIA-SMI 410.73       Driver Version: 410.73       CUDA Version: 10.0     |
|-------------------------------+----------------------+----------------------+
| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|===============================+======================+======================|
|   0  Tesla M40           Off  | 00000000:89:00.0 Off |                  N/A |
| N/A   26C    P8    16W / 250W |      0MiB / 11448MiB |      0%      Default |
+-------------------------------+----------------------+----------------------+

+-----------------------------------------------------------------------------+
| Processes:                                                       GPU Memory |
|  GPU       PID   Type   Process name                             Usage      |
|=============================================================================|
|  No running processes found                                                 |
+-----------------------------------------------------------------------------+
agartner@test:~$ gpu release
OK
agartner@test:~$ nvidia-smi
No devices were found
agartner@test:~$
```

The `gpu info` command will show you who is using GPUs. Here's some sample output:

```shell
agartner@test:~$ gpu info
+-----------+----------+-----+
| CONTAINER |   USER   | GPU |
+-----------+----------+-----+
| test      | agartner | 6   |
+-----------+----------+-----+
```

## Timeouts

If you do not use a GPU for five minutes, it will be released back into the resource pool. You can adjust this timeout via the `-t` option. In the following example, your lease would timeout after an hour:

```shell
gpu get -t 1h 1
```

Valid time units are `h` (hours), `m` (minutes), and `s` (seconds)

## Devices and Affinity

You may want to select specific devices if the system has different types of GPUs or you are concerned about your CPU affinity. Unfortunately, the resource manager doesn't allow you to select specific devices. One workaround is to get all the devices on the system, then only use the ones you require. The unused devices will timeout and return to the pool after five minutes.

## Slack Integration

If configured, the you can use the `gpu done` command to send yourself a slack message when run. You must be registered on the organizational slack. Here's an example:

```shell
python train.py; gpu done
```